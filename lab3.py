# -*- coding: utf8 -*-
def mycode():
    import math
    try:
        a = float(input("Введите а "))
        x = float(input("Введите x "))
        c = int(input("Введите команду 1-g, 2-f, 3-y. "))
        n = int(input("Количество шагов вычисления "))
    except ValueError:
        print("Ввод некорректен")
        exit(1)
    j = 0
    print("Для выхода из цикла нажмите ctrl+C")
    if c == 1:
        try:
            for i in range(n):
                g = (5 * (10 * a ** 2 - 11 * a * x + x ** 2)) / (24 * a ** 2 - 49 * a * x + 15 * x ** 2)
                x += 0.2
                j += 1
                print(j, g)
        except ZeroDivisionError:
            print("Деление на 0")
            exit(1)
    elif c == 2:
        try:
            for i in range(n):
                f = math.sinh(3 * a ** 2 + 7 * a * x + 4 * x ** 2)  # гиперболический синус
                x += 0.2
                j += 1
                print(j, f)
        except ValueError:
            print("Не входит в область определения")
            exit(1)
    elif c == 3:
        try:
            for i in range(n):
                y = -math.atanh(30 * a ** 2 + 37 * a * x - 4 * x ** 2)  # гиперболический арктангенс
                x += 0.2
                j += 1
                print(j, y)
        except ValueError:
            print("Не входит в область определения")
            exit(1)
    else:
        print("Комманда не выбрана или выбрана неправильно")

mycode()
while True:
    again = input("Хотите начать программу сначала? [Y/N]: ")

    if again == "Y" or "y":
        mycode()
    else:
        exit(1)


